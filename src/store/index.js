import Vue from "vue";
import Vuex from "vuex";
import superHeroesService from "@/services/superHeroesService";
import * as heroes from "@/store/modules/heroes.js";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    skills:null,
  },
  mutations: {
  SET_SKILLS(state, skills){
    state.skills = skills
  },
  ADD_SKILL(state, data){
    state.skills.push(data)
  },
  REMOVE_SKILL(state, id){
    let skills = state.skills.filter(skill => skill.id != id);
    state.skills = skills;
  },
},
  actions: {
  async fetchSkills({commit}){
    let response = await superHeroesService.getSkills()
    let skills = response.data.skills;
    commit('SET_SKILLS', skills)
  
  },
  async addSkill({commit}, data){
    let response = await superHeroesService.postSkill(data);
    let newSkill = response.data;
    commit('ADD_SKILL', newSkill)
    
  },
  async removeSkill({commit}, id){  
   let response = await superHeroesService.deleteSkill(id)
   if(response.status == 200){
      commit('REMOVE_SKILL', id)
    }
  },
},
getters:{},
modules: { heroes }
});
