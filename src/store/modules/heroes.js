import superHeroesService from "@/services/superHeroesService"


export const namespaced = true;

export const state = {
  	heroes:null,
	  hero:null,
    currentPage:1,
    perPage:4,
    total:null
  };

export const mutations = {
  SET_TOTAL(state, total){
    state.total = total
  },
  SET_CURRENT_PAGE(state, currentPage){
    state.currentPage = currentPage
  },
	SET_HEROES(state, heroes) {
		state.heroes = heroes
	},
	SET_HERO(state, hero) {
		state.hero = hero
  	},
  REMOVE_HERO(state, id){
    let heroes = state.heroes.filter(hero => hero.id != id);
    state.heroes = heroes;
  },
  EDIT_HERO(state, data) {
      let heroes = state.heroes.map(h => {
        if(h.id == data.id){
          return data
        }
        return h
      })
      state.heroes = heroes;
    },
};

export const actions = {
  fetchHeroes({ commit }, page) {
    
  	superHeroesService.getHeroes(page).
    	then(res =>{
        console.log(page, "DONE")
        console.log(res.data, "page")
        commit('SET_CURRENT_PAGE', res.data.current_page)
        commit('SET_TOTAL', res.data.total)
    		commit('SET_HEROES', res.data.data)
    	})
      .catch(err => {
        console.log("hero's error", err)
      });
      
  },
  fetchHero({ commit, getters }, id) {
    var hero = getters.getHeroById(id)

    if (hero) {
      commit('SET_HERO', hero)
      return hero
    } else {
      return superHeroesService.getHero(id)
        .then(response => {
          commit('SET_HERO', response.data)
          return response.data
        })
    }
  },
  addHero({dispatch}, hero){
    superHeroesService.postHero(hero)
    .then(() => {
      dispatch('fetchHeroes', 1)
    }).catch(err => {
      console.log(err)
    })
  },
  async removeHero({commit}, id){
    
   let response = await superHeroesService.deleteHero(id);
   if(response.status == 200){
    commit('REMOVE_HERO', id)
   }
   
  },
  async updateHero({commit}, data){
    let skills = data.skills.map(skill => skill.id)
    await superHeroesService.editHero(data.id, {
      id: data.id,
      name:data.name,
      birthdate:data.birthdate,
      skills: skills
    });
      commit('EDIT_HERO', data)
  }
};

export const getters = {
	getHeroById: state => id => {
    return state.heroes.find(hero => hero.id == id)
  }
};

