import Vue from "vue";
import VueRouter from "vue-router";
import HeroesList from "../views/HeroesList.vue";
import CreateHero from "../views/CreateHero.vue";
import CreateSkill from "../views/CreateSkill.vue";
import EditHero from "../views/EditHero.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "heroes-list",
    component: HeroesList
  },
  {
    path: "/hero-show/:id",
    name: "hero-show",
    props:true,
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "hero-show" */ "../views/HeroShow.vue")
  },
  {
    path: "/create-hero",
    name: "create-hero",
    component: CreateHero
  },
  {
    path: "/create-skill",
    name: "create-skill",
    component: CreateSkill
  },
  {
    path: "/hero-edit",
    name: "hero-edit",
    component: EditHero,
    props:true
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
