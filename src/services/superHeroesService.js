import axios from "axios";


const apiClient = axios.create({
    baseURL:"https://superhero.test.aliftech.uz/api/",
    withCredentials:false,
    headers:{
        Accept:"application/json",
        'Content-Type':"application/json"
    },
    timeout:10000
});

// apiClient.interceptors.request.use( config =>{
//     return config;
// })

// apiClient.interceptors.response.use(response =>{
//     return response;
// })

export default{
    getHeroes(page){
        return apiClient.get("heroes?per_page=4&page=" + page);
    },
    getHero(id){
        return apiClient.get("heroes/" + id);
    },
    getSkills(){
        return apiClient.get("app")
    },
    postHero(data){
        return apiClient.post("heroes", data);
    },
    editHero(id, data){
        return apiClient.put(`heroes/${id}`, data);
    },
    deleteHero(id){
        return apiClient.delete("heroes/" + id)
    },
    postSkill(data){
        return apiClient.post("skills", data)
    },
    deleteSkill(id){
        return apiClient.delete("skills/" + id)
    }
   
}